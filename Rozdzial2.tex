\chapter[Wprowadzenie teoretyczne (Broda Szymon)]{Wprowadzenie teoretyczne}\label{2}
Pierwsze zdjęcie satelitarne zostało wykonane przez amerykańskiego satelitę Explorer 6 w 1959 roku \cite{Explorer}. Od tego czasu aspekt obserwacji ziemi rozwija się w coraz to szybszym tempie, a liczba satelitów znajdujących się w przestrzeni kosmicznej nieustannie rośnie.  Na orbity wystrzeliwane są zarówno satelity rządowe, jak i komercyjne, które rejestrują i zapisują ogromne ilości danych. Wraz ze wzrostem dostępu do tych danych, powiększa się również obszar możliwego ich wykorzystania. Niesie to za sobą silne zapotrzebowanie na rozwój technologii związanej ze specjalistyczną interpretacją treści satelitarnych. 
	
Przez długi czas największym problemem napotykanym podczas opracowywania różnych metod interpretacji zdjęć satelitarnych, był brak odpowiedniego narzędzia pozwalającego na sprawną analizę tych treści. Do 2012 roku w dziedzinie teledetekcji stosowano głównie techniki algorytmiczne, które były niezwykle obliczeniochłonne i mało efektywne. W 2012 roku nastąpił przełom, jakim była rewolucja głębokiego uczenia się, która pozwoliła na bardziej wydajne podejście do problemu.
	
Nowoczesne techniki uczenia maszynowego, a w szczególności głębokie uczenie sprawiły, że zadania takie jak wykrywanie obiektów, ich liczenie, segmentacja semantyczna oraz ogólna klasyfikacja obrazów stały się zdecydowanie łatwiejsze do zaimplementowania. Innym udogodnieniem był proces nadzorowanego uczenia, który pozwalał na wytrenowanie modelu poprzez wykorzystanie oznakowanych etykietami obrazów.
	
W przeciwieństwie do zdjęć niewielkich rozmiarów zapisywanych w standardowym formacie takim jak PNG bądź JPEG, które cechują się trzema kanałami w widmie widzialnym, zdjęcia satelitarne mogą zawierać kilkanaście kanałów (z których większość nie jest w widmie widzialnym) i przechowywane są w formatach przestrzennych. Stąd też w przypadku próby interpretacji bardziej złożonych zdjęć satelitarnych z użyciem typowych modeli uczenia maszynowego, zmuszeni jesteśmy do zmiany ich architektury, bądź przekształcenia zdjęć do wymaganego formatu, co może powodować realne przekłamania \cite{Morrison, EO}.


\section{Obserwacja Ziemi}
Jednym z najbardziej istotnych czynników określających jakość satelitarnej teledetekcji jest rozdzielczość przestrzenna, określająca dokładność czujnika. W przypadku satelity o rozdzielczości 1 metra, każdy piksel mozaiki odpowiada powierzchni 1 metra na Ziemi, podczas gdy rozdzielczość 10 metrów odpowiada 10 metrom kwadratowym na Ziemi. Wpływ na wynikową rozdzielczość obrazu ma jakość wykorzystywanego teleskopu oraz odległość satelity od planety, wraz z jej zwiększaniem, rozdzielczość maleje, rośnie czas transmisji, ale powiększa się obszar możliwy do pokrycia.
	 
\subsection{Rodzaje czujników wykorzystywanych do obserwacji Ziemi}
Obecna technologia obserwacji Ziemi wykorzystuje trzy rodzaje czujników: radar filmowy, radar elektrooptyczny oraz syntetyczny radar z aparturą SAR \cite{Dehqanzada}. 
 	
Pierwszy z nich, radar filmowy wykonuje rzeczywiste zdjęcia charakteryzujące się wysoką rozdziel-czością, jednak w celu pozyskania filmu konieczny jest bezpośredni dostęp do danych, realizowany poprzez wystrzeliwanie kapsuł filmowych lub fizyczny kontakt z satelitą. Wadą takiego rozwiązania jest również ograniczony zasób nośników pamięci możliwych do zainstalowania w tych urządzeniach, co powoduje, iż satelita staje się bezużyteczny po jego wykorzystaniu. Prowadzi to do konieczności częstego wysyłania kolejnych satelitów. Satelity wyposażone w czujniki filmowe były głównie stosowane w celach szpiegowskich przez rząd radziecki oraz amerykański.
 	
Czujniki elektrooptyczne mierzą promieniowanie elektromagnetyczne odbite lub emitowane przez obiekty na powierzchni Ziemi, tworząc cyfrowe obrazy, które są następnie przesyłane do stacji odbiorczych na Ziemi. Jednak podobnie jak czujniki filmowe nie generują one własnych sygnałów, dlatego są zależne od innych źródeł energii, takich jak Słońce, niezbędnych do oświetlania obserwowanych obiektów. Wykorzystanie czujników elektrooptycznych ograniczone jest przez warunki atmosferyczne oraz porę dnia. Istnieją trzy rodzaje czujników elektrooptycznych: 
\begin{itemize}
  	\item panchromatyczne - tworzą obrazy czarno-białe, wykrywając współczynnik odbicia tylko w jednym paśmie
  	\item wielospektralne - mogą tworzyć kolorowe obrazy, odczytując odbicie elektromagnetyczne w różnych pasmach kolorów
  	\item hiperspektralne - wysoce specjalistyczne czujniki zdolne do rozróżniania dziesiątek a nawet setek odcieni kolorów.
\end{itemize}
  
Ostatnim wspomnianym typem czujników są czujniki radarowe z syntetyczną aperturą, zdolne do pracy w każdych warunkach pogodowych oraz niezależne od pory dnia. Emitują własne sygnały w mikrofalowej części widma, a następnie odbierają charakterystykę sygnału powrotnego po odbiciu od obiektów na powierzchni. Podobnie jak w przypadku systemów elektrooptycznych wysyłają do naziemnych stacji odbiorczych dane w formie cyfrowej.

\subsection{Orbity ziemskie}
Trzy najbardziej popularne orbity po których poruszają się satelity zostały przedstawione na rysunku \ref{fig:scheme3}. Wśród nich wyróżniamy LEO \textit{(ang. Low Earth Orbit}), MEO \textit{(ang. Medium Earth Orbit)} oraz GEO \textit{(ang. Geosynchronous Earth Orbit)} \cite{EO, Morrison}.
	
\begin{figure}[h]
	\centering
	\includegraphics[width=12cm]{img/scheme3}
	\caption[System orbit]{System orbit. Kolorem zielonym jest oznaczona niska orbita ziemska, żółtym, średnia orbita ziemska, natomiast kolorem czerwonym, geosynchroniczna orbita ziemska.}
	\label{fig:scheme3}
\end{figure}	
	
Za niską orbitę ziemską przyjmuje się wszystkie potencjalne orbity znajdujące się w odległości poniżej 2000 km od powierzchni ziemi. Satelity krążące w LEO wykonują pełny cykl  w czasie średnio od 90 do 120 minut. Mała wysokość sprawia, że satelity są w stanie kontrolować niewielki obszar, jednak ich względna mobilność oraz krótki czas transmisji gwarantują idealne warunki do dynamicznej obserwacji Ziemi i rozpoznania. Średnia orbita ziemska znajduje się w odległości około 20000 kilometrów od powierzchni Ziemi. Satelity znajdujące się na MEO stanowią swego rodzaju kompromis pomiędzy obserwowanym obszarem, a wymaganą rozdzielczością i oczekiwanym czasem transmisji. Satelity znajdujące się na orbicie GEO oddalone są od Ziemi o około 36000 kilometrów, a wykonanie pełnego cyklu zajmuje im w przybliżeniu jeden dzień. Ich cechą charakterystyczną jest to, że pozycja Słońca względem satelity i Ziemi pozostaje niezmienna.  


\section{Uczenie maszynowe}
Po raz pierwszy termin uczenia maszynowego (ML - ang. \textit{ Machine Learning}) został użyty przez Arthura Samueala w 1959 roku i oznaczał on według autora możliwość uczenia się komputerów bez wyraźnego programowania \cite{Sammuel A}. Rozwijając te słowa, algorytm uczenia maszynowego polega na budowaniu modelu matematycznego w oparciu o otrzymywane dane szkoleniowe i wykorzystywanie go do analizy nowych danych, a także poszukiwania coraz to lepszych rozwiązań badanego problemu.
	
	Rodzaje algorytmów uczenia maszynowego można rozróżniać na podstawie podejścia, rodzaju wprowadzanych i wysyłanych danych oraz specyfikacji rozwiązywanego zadania lub problemu. Podstawowe metody to: 

\begin{itemize}
  	\item uczenie bez nadzoru:
	\begin{itemize}
  		\item klasteryzacja to grupowanie danych ze względu na podobieństwo cech przy pomocy określonej metryki.
  		\item redukcja wymiarów to kompresja liczby cech opisujących dany zbiór danych. Polega ona na rzutowaniu ich do podprzestrzeni składającej się z mniejszej liczby wymiarów, gdzie każda cecha jest wymiarem częściowo 						reprezentującym opisywany obiekt.
	\end{itemize}
  	\item uczenie nadzorowane:
	\begin{itemize}
  		\item klasyfikacja to przypisywanie zdefiniowanych wcześniej etykiet o charakterze dyskretnym dla wprowadzanych danych wejściowych.
  		\item regresja to przewidywanie ciągłych wartości wyjściowych na podstawie otrzymywanych wartości wejściowych.
	\end{itemize}
		  \item uczenie ze wzmocnieniem.
\end{itemize}

\subsection{Uczenie nadzorowane}
Algorytmy uczenia nadzorowanego aktualizuje wagi sieci na podstawie otrzymywanych danych, składających się z danych treningowych i oczekiwanego wyjścia. Takie podejście zapewnia możliwość korekcji modelu poprzez iteracyjną optymalizację funkcji celu, wykorzystywanej do przewidywania wyników dla nowych danych wejściowych. Rysunek \ref{fig:scheme4} przedstawia schemat działania algorytmu uczenia się z nadzorem. 

\begin{figure}[h]
	\centering
	\includegraphics[width=14cm]{img/scheme4}
	\caption{Schemat działania algorytmu uczenia się nadzorowanego.}
	\label{fig:scheme4}
\end{figure}

\subsection{Uczenie bez nadzoru}
Algorytm uczenia bez nadzoru opiera się na rozpoznawaniu wzorców pomiędzy otrzymywanymi zestawami danych, przy braku jakichkolwiek informacji na temat pożądanej odpowiedzi. Jest wykorzystywany głównie do wyszukiwania powiązań pomiędzy danymi i ich grupowania. Rysunek \ref{fig:scheme5} przedstawia schemat działania algorytmu uczenia się bez nadzoru.

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm]{img/scheme5}
	\caption{Schemat działania algorytmu uczenia się nienadzorowanego.}
	\label{fig:scheme5}
\end{figure}

\subsection{Uczenie ze wzmocnieniem}
Uczenie ze wzmocnieniem to sposób uczenia maszynowego, którego celem jest wytrenowanie modelu w taki sposób, aby podejmował on sekwencję decyzji. Model nie otrzymuje żadnych wskazówek dotyczących sposobu rozwiązywania problemu, a sam algorytm wykorzystuje metodę prób i błędów, ucząc się taktyki działania. Za każdy wykonany ruch maszyna jest nagradzana lub karana, a jej zadaniem jest maksymalizacja zysku. Na rysunku \ref{fig:scheme6} przedstawiony został schemat szkolenia modelu metodą uczenia ze wzmocnieniem.

\begin{figure}[h]
	\centering
	\includegraphics[width=12cm]{img/scheme6}
	\caption{Schemat działania uczenia ze wzmocnieniem.}
	\label{fig:scheme6}
\end{figure}


\section{Sieci neuronowe}
Struktura i funkcja sztucznych sieci neuronowych (ANN - ang. \textit{Artificial Neural Networks}) jest oparta na wykształconym w zeszłym wieku zrozumieniu biologicznego układu nerwowego. Według jednej z definicji "\textit{Obliczenia neuronowe to badanie sieci elastycznych węzłów, które poprzez proces uczenia się z zadań ewoluują, przechowując wiedzę empiryczną i udostępniając ją do wykorzystania}" \cite{Morton}. Oznacza to, że sieci zbudowane są za pomocą dużej liczby prostych i elastycznych jednostek przetwarzających (PU - ang. \textit{Processing Unit}). Połączone są one ze sobą w taki sposób, że mogą przechowywać wiedzę doświadczalną poprzez uczenie się na przykładach, aby następnie na podstawie zbioru zebranych wcześniej informacji i~powiązań znaleźć rozwiązanie zbliżone do spotykanych wcześniej problemów bez stosowania wyraźnego zestawu reguł.  

\subsection{Historia sieci neuronowych}
Prace związane z sieciami neuronowymi rozpoczęły się w latach 40. XX wieku. W roku 1943 McCulloch i Pitts, opierając się na biologicznych strukturach, wyprowadzili pierwszy matematyczny model komórki nerwowej \cite{McCulloch}. Model ten następnie został rozwinięty rozwinięty przez Hebba, który rozszerzył ten pomysł definiując metodę uczenia sieci neuronowej poprzez aktualizację wag synaptycznych \cite{Hebb D}. Natomiast pierwsza, prymitywna sieć neuronowa nazwana \textit{Perceptron} została opisana przez Rosenblatta w 1958 roku~\cite{Rosenblatt F}. 
	
Kolejna, uważana za jedną z najważniejszych praca związanya z technologią sieci neuronowych, została opublikowana w roku 1960  przez Widrowa i Hoffa \textit{„Adaptive Switching Circuits”} \cite{Widrow B}. Poza zaprojektowaniem narzędzia sieci neuronowej, które następnie symulowali na komputerach, zaimplementowali swoje projekty jako fizyczną maszynę, tworząc urządzenie o nazwie „\textit{ADALINE}” (ang. \textit{Adaptive Linear Neuron}). Genialny okazał się także stworzony przez nich algorytm Widrowa-Hoffa, wykorzystany do nauki "\textit{ADALINE}", powszechnie znany jako algorytm LMS (ang. \textit{Least Mean Squares}).

Dziedzina sztucznej inteligencji rozwijała się prężnie aż do 1969 roku, kiedy to Minski i Papert wydali książkę "\textit{Perceptrons}", w której przeprowadzając dokładną, matematyczną analizę perceptronów, dowiedli, iż prymitywna jednowarstwowa sieć nie będzie w stanie rozwiązać bardziej skomplikowanych problemów \cite{Minsky M}. Po tej publikacji zainteresowanie tą technologią gwałtownie spadło, jednak cześć naukowców wciąż kontynuowała swoje badania. I tak Wilshaw, Buneman i Longuet-Higgins pracowali nad korelograficznym modelem, stanowiącym alternatywę dla holograficznego modelu Gabora, który następnie rozszerzyli do sieci asocjacyjnej. Werbos opisał metodę propagacji wstecznej błędu, a Kohonen przedstawił model pamięci asocjacyjnej, który w dalszych badaniach wykorzystał do stworzenia mapy Kohonena zwanych również SOM (ang. \textit{Self-Ogranizing Map}) \cite{Kohonen T}.
	
Ponowny wzrost zainteresowaniem sieciami neuronowymi nastąpił w roku 1982, po tym jak Hopfield opublikował wykreowany przez siebie model sieci Hopfielda, który w dalszych badaniach wykorzystali Ackley, Hinton oraz Sejnowski tworząc maszynę Boltzmana. Maszyna Boltzmana umożliwiła koncepcyjne odejście od idei sieci neuronowej jako pasywnej maszyny asocjacyjnej w kierunku postrzegania sieci neuronowej jako modelu generatywnego \cite{Hopfield J}.	Impuls dostarczony przez maszynę Boltzmana przyczynił się do powstania bardziej praktycznej metody opracowanej w 1986 roku przez Rumelharta, Hintona i Williamsa, nazwanej algorytmem uczenia się propagacji wstecznej \cite{D.Rumelhart}. Sieć propagacji wstecznej składająca się z trzech warstw była zdolna do uczenia się skomplikowanych zadań w sposób podobny do ludzkiego, a jej wykorzystanie pozwoliło na stworzenie \textit{NETtalk}, sieci która potrafiła tłumaczyć tekst na mowę.
 
Pod koniec wieku XX technologia związana z sieciami neuronowymi rozwijała się w coraz szybszym tempie. W 1999 roku po raz pierwszy zastosowano procesor graficzny GPU (\textit{ang. Graphical Processing Unit}), który zwiększył prędkość obliczeń skracając tym samym proces nauki tworzonych modeli sieci. Wraz z początkiem kolejnego wieku można zaobserwować dynamicznie rozwijającą się dziedzinę sztucznej inteligencji wzorującą się na działaniu ludzkiego mózgu, a z każdym kolejnym dniem pojawiają się co raz to nowe pomysły na jej wykorzystanie.

	
\subsection{Budowa neuronu}	 
Sieć neuronowa zbudowana jest z jednostek przetwarzania (PU), które naśladują funkcję neuronu biologicznego w mózgu. Jednostki PU są logicznym urządzeniem przetwarzającym, którego podstawową funkcją jest wytwarzanie sygnału wyjściowego jako funkcji sumy ich ważonych wejść i pewnej wartości progowej \cite{B WU}. Matematycznie opisane jest to zależnością:
	 
\begin{equation}
	 y_{i} = f_{i}(\sum_{j=1}^{n} w_{ij}x_{j} + s_{i})
	 \label{eq:eq1}
\end{equation}

gdzie $y_{i}$ reprezentuje sygnał wyjściowy i-tej jednostki przetwarzającej PU$_{i}$, $w_{ij}$ reprezentuje wagi pomiędzy połączeniami z $j$-tych jednostek do jednostek $i$-tych, a $s_{i}$ stanowi wartość progową (ang. \textit{bias}) PU$_{i}$. Natomiast $f_{i}$ jest funkcją aktywacji i-tej jednostki. Graficzny schemat jednostki przetwarzania został przedstawiony na rysunku \ref{fig:scheme2}.
	 
\begin{figure}[h]
	\centering
	\includegraphics[width=11cm]{img/scheme2}
	\caption{Schemat budowy sztucznego neuronu}
	\label{fig:scheme2}
\end{figure}	

Wejście do PU$_{i}$ może być wyjściem z innych PU lub bezpośrednio z zewnątrz sieci neuronowej jako dane wejściowe, a wyjście PU$_{i}$ może być wykorzystane jako wejście do kolejnych PU lub jako wyjście ANN. Wartość wagi $w_{ij}$ określa, jak silnie wydajność PU$_{j}$ wpływa na aktywność PU$_{i}$ i może być zmieniana w czasie. Próg $s_{i}$ działa jako filtr dla sygnałów przychodzących, natomiast $f_{i}$  jest określane mianem funkcji aktywacji, odpowiedzialnej za określenie wielkości aktualnego sygnału wyjściowego. Najprostszymi funkcjami aktywacji są funkcje skokowa i liniowa, natomiast często wykorzystywane są również bardziej złożone funkcje takie jak funkcja Sigmoid, oraz ReLu \textit{(ang. Rectifield Linear Units)}, które zyskało swoją popularność w dziedzinie głębokich sieci neuronowych.
	
\subsection{Wielowarstwowe sieci neuronowe}\label{WSN}
Cechą charakterystyczną sieci wielowarstwowej jest posiadanie przynajmniej jednej warstwy ukrytej, pośredniczącej w komunikacji między danymi na wejściu i wyjściu. W każdej warstwie może znajdować się różna liczba neuronów, natomiast w obrębie jednej warstwy mogą znajdować się jedynie niepołączone ze sobą neurony tego samego typu (wyjątkiem są rekurencyjne sieci neuronowe), przy czym pomiędzy warstwami, najczęściej obowiązuje zasada każdy z każdym. Przykładowa sieć neuronowa złożona z trzech warstw została przedstawiona na rysunku \ref{fig:scheme1}.
	
\begin{figure}[h]
	\centering
	\includegraphics[width=10cm]{img/scheme1}
	\caption{Schemat architektury wielowarstwowej sieci neuronowej.}
	\label{fig:scheme1}
\end{figure}	
	
Wyróżniamy trzy typy warstw. Pierwszym z nich jest warstwa wejściowa, która przyjmuje numery-czną reprezentację danych i przekazuje je do pierwszej warstwy ukrytej. Kolejnym typem jest wspomniana warstwa ukryta, odpowiedzialna  za naukę i większość obliczeń, do której nie można przekazywać informacji w sposób bezpośredni. Ostatnią warstwą jest warstwa wyjściowa, której zadaniem jest obliczenie wartości wyjściowej i wystawienie jej na wyjściu modelu.

		
\section{Uczenie głębokie}
Uczenie głębokie (DL - ang. \textit{Deep Learning}) polega na automatycznym uczeniu się wielu poziomów reprezentacji podstawowego rozkładu danych, które mają być modelowane \cite{Lauzon}. Innymi słowy, algorytm głębokiego uczenia w sposób automatyczny wyodrębnia funkcje niskiego i wysokiego poziomu niezbędne do klasyfikacji. Funkcja wysokiego poziomu jest cechą, która hierarchicznie zależy od innych funkcji. W kontekście graficznej interpretacji obrazu zależność ta oznacza, że algorytm głębokiego uczenia się wytworzy własne reprezentacje niskiego poziomu na podstawie surowego obrazu (takiego jak detektor krawędzi, filtry Gabora itp.), a następnie zbuduje reprezentacje zależne od tych reprezentacji niskiego poziomu (takie jak liniowe kombinacje tych reprezentacji niskiego poziomu) i sukcesywnie będzie powtarzał ten sam proces dla wyższych poziomów. Niezbędną cechą głębokich sieci neuronowych, stosowanych w głębokim uczeniu się, jest posiadanie rozbudowanej części obliczeniowej, która uzyskiwana jest poprzez tworzenie sieci składających się z wielu warstw ukrytych.
		
Algorytmy głębokiego uczenia wykazały się lepszymi wynikami podczas procesu uczenia się poprzez transfer oraz klasyfikację w takich obszarach jak między innymi, rozpoznawanie odręcznych cyfr, kategoryzacja obiektów, wykrywanie pieszych lub nawigacja za pomocą robotów terenowych, a także w sygnałach akustycznych podczas klasyfikacji audio \cite{DL Intro}. W przetwarzaniu języka naturalnego bardzo interesujące podejście przedstawione został w \cite{Collobert} stanowi dowód na to, że głębokie architektury mogą wykonywać wielozadaniowe uczenie się, dając rewelacyjne wyniki w trudnych zadaniach, takich jak analiza składniowa zdań.
	
	
\section{Uczenie transferowe}
Dziedzina eksploracji danych i uczenia maszynowego jest szeroko i z powodzeniem wykorzystywana w wielu zastosowaniach, w których można wyodrębnić wzorce z przeszłych informacji w celu przewidzenia przyszłych wyników. W wielu rzeczywistych zastosowaniach zebranie potrzebnych danych szkoleniowych i budowanie modeli może być trudne i kosztowne. W takich przypadkach pożądany jest transfer wiedzy lub transfer uczenia się między powiązanymi domenami zadań \cite{Weiss}.
	
Rozwiązaniem tego problemu może być zastosowanie uczenia transferowego, polegającego na wykorzystaniu modelu sieci o takiej samej bazowej architekturze, który wytrenowany został wcześniej na ogromnym zbiorze danych i posiada już pewną wiedzę z zakresu opracowywanego zadania. W ten sposób zamiast inicjować losowe parametry (wagi i wartości progowe) dla każdej warstwy naszej sieci, używamy zestawu początkowych parametrów zdefiniowanych już w bazowym modelu. Aby skorzystać z tego rozwiązania należy usunąć warstwę wyjściową podstawowej architektury i zamiast tego dołączyć dodatkowe warstwy, które pozwolą na adaptację oryginalnego modelu i dostosowanie go do określonego zadania.
	
Przykładem zastosowania uczenia transferowego może być problem klasyfikacji obrazu, gdzie celem jest rozróżnienie ras psów. Zakładając, że pierwotny model posiada zdolność wykrywania tego gatunku zwierząt na obrazach, istnieje możliwość wykorzystania wiedzy zdobytej dla bardziej ogólnego zadania w celu rozwiązywania problemu bardziej wyspecjalizowanego, jakim jest rozróżnianie konkretnych ras psów. 
	
Transfer uczenia się jest kluczem do przełomu w technikach głębokiego uczenia się w wielu środowiskach zajmujących się małymi danymi. Uczenie głębokie występuje powszechnie we współczesnych badaniach, ale wiele rzeczywistych scenariuszy nie posiada odpowiednio dużego zbioru oznaczonych danych. Techniki głębokiego uczenia, a w szczególności metody uczenia nadzorowanego, wymagają ogromnych ilości informacji w celu dostrojenia milionów parametrów w sieci neuronowej, co często wiąże się z wysokimi kosztami pozyskania niezbędnych danych oraz obliczeniochłonnym procesem treningu sieci. Korzystając z bardziej uogólnionego, wytrenowanego wcześniej modelu, powstaje możliwość przeniesienia wiedzy i stworzenia modelu lepiej wyspecjalizowanego w konkretnej dziedzinie. 

\subsection{Uczenie wielozadaniowe}
W uczeniu się wielozadaniowym model trenowany jest do wielu zadań w tym samym czasie. Zazwyczaj podczas tego procesu używane są modele głębokiego uczenia się, ponieważ można je elastycznie dostosowywać. Schemat uczenia wielozadaniowego został zaprezentowany na rysunku \ref{fig:scheme8}. Architektura sieci jest dostosowywana w taki sposób, że pierwsze warstwy są używane we wszystkich zadaniach, po których następują wyspecjalizowane warstwy, specyficzne dla  konkretnego problemu. Główną zaletą takiego podejścia jest to, że dzięki szkoleniu sieci w zakresie różnych zadań, sieć taka będzie lepiej uogólniać, gdyż będzie ona wykorzystywała wiedzę nabytą podczas rozwiązywania zagadnień podobnych.

\begin{figure}[h]
	\centering
	\includegraphics[width=10cm]{img/scheme8}
	\caption{Schemat architektury uczenia wielozadaniowego.}
	\label{fig:scheme8}
\end{figure}	


\section{Splotowe sieci neuronowe}\label{CNN}
Splotowe sieci neuronowe (CNN - ang. \textit{Convolutional Neural Networks}) są specjalistycznymi wielowarstwowymi sieciami neuronowymi (szerzej opisanymi w sekcji \ref{WSN}) wykorzystywanymi głównie w rozpoznawaniu wzorców na obrazie. Wiedza z zakresu ich przeznaczenia umożliwia zakodowanie specyficznych dla obrazu funkcji w architekturze, dzięki czemu sieć jest skuteczniejsza podczas pracy z obrazem, a jednocześnie pozwala na zredukowanie parametrów wymaganych do skonfigurowania modelu \cite{Albawi}. Przykładowa architektura splotowej sieci neuronowej została przedstawiona na rysunku \ref{fig:scheme7}.
	
\begin{figure}[h]
	\centering
	\includegraphics[width=14cm]{img/scheme7}
	\caption{Przykładowa architektura splotowej sieci neuronowej.}
	\label{fig:scheme7}
\end{figure}

Struktura CNN składa się z trzech rodzajów warstw \cite{Oshea}: konwolucyjnych, redukujących oraz w pełni połączonych. Warstwy konwolucyjne (ang. \textit{convolutional layer}) inaczej nazywane też warstwami splotowymi, są podstawą sieci CNN. Splatają one dane wejście i przekazują wynik do następnych warstw, przy czym każdy neuron konwolucyjny przetwarza dane dla lokalnego regionu w wejściowym obrazie zwanym polem receptywnym. Warstwy redukujące (ang. \textit{pooling layer}) są odpowiedzialne za zmniejszanie rozmiaru danych i usprawnianie podstawowych obliczeń, poprzez łączenie wyników klastrów neutronów w jedne warstwie w pojedynczy neuron w warstwie kolejnej. Ostatnie z wymienionych,  warstwy w pełni połączone (ang. \textit{fully connected layer}) wiążą każdy neuron w jednej warstwie z każdym neuronem w warstwie kolejnej. Odpowiadają one za właściwą klasyfikację i stosowane są zazwyczaj na końcu sieci CNN.
	
Istotą konwolucyjnych sieci neuronowych są wspomniane warstwy splotowe składające się z określonej liczby filtrów, które służą do wykrywania wzorców. Początkowo wykrywane mogą być różnego rodzaju krawędzie, a wraz z zagłębianiem się w kolejne warstwy sieci i propagacją informacji, wykorzystywane filtry stają się coraz bardziej wyspecjalizowane w wyodrębnianiu określonych obiektów. W wyniku nakładania filtru na kanał wejściowy (inaczej obraz wejściowy) powstaje kanał wyjściowy, którego rozmiary są zmniejszone w zależności od wielkości użytego filtra oraz wielkości zastosowanego kroku (ang. \textit{stride}), co zostało przedstawione na rysunku \ref{fig:filters}.

\begin{figure}[h]
	\centering
	\begin{subfigure}{.5\textwidth}
		\includegraphics[width=6.5cm]{img/filters}
		\caption{Filtra o wielkości 3x3, wielkość kroku = 1.}
	\end{subfigure}\hfill
	\begin{subfigure}{.5\textwidth}
		\includegraphics[width=6.5cm]{img/filters2}
		\caption{Filtr o wielkości 2x2, wielkość kroku = 2.}
	\end{subfigure}\hfill
	\caption[Schemat działania filtru w sieci CNN]{Schemat przedstawiający różnicę w wielkości kanału wyjściowego w zależności o zastosowanego rozmiaru filtru i wielkości kroku.}
	\label{fig:filters}
\end{figure}
	
Analizując działanie filtrów wykorzystywanych w warstwach splotowych można zauważyć, że rozmiar kanału wyjściowego ulega zmniejszeniu, zależnemu od wymiarów zastosowanego filtra. W celu zapobiegnięcia utraty oryginalnego rozmiaru danych wejściowych stosowana jest metoda wypełniania zerami (ang. \textit{zero padding}). Polega ona  na obramowaniu obrazu wejściowego pikselami o wartości zero. W ten sposób zagwarantowana zostaje podstawowa możliwość manipulowania rozmiarem kanału wyjściowego przy jednoczesnym zachowaniu przenoszonych informacji.
	
Konwolucyjne sieci neuronowe różnią się od innych form sztucznych sieci neuronowych tym, że zamiast skupiać się na całej domenie problemowej, wykorzystują wiedzę o określonym typie danych wejściowych. To z kolei pozwala na znacznie prostszą architekturę sieci, w której najważniejszym elementem jest warstwa splotu. Wydajność sieci zależy od liczby poziomów z których jest złożona, a wraz ze zwiększaniem liczby warstw zwiększa się także czas potrzebny do wytrenowania sieci. Obecnie CNN uważa się za skuteczne narzędzie w dziedzinie uczenia maszynowego w wielu zastosowaniach, takich jak wykrywanie twarzy, rozpoznawanie wideo i rozpoznawanie głosu.


\section{Warunkowe pola losowe CRF}\label{CRF}
Warunkowe pola losowe (CRF – ang. \textit{Conditional Random Fields}) to klasa modeli dyskryminacyjnych, w których informacje kontekstowe lub stan sąsiadów wpływają na bieżącą prognozę.
Podczas gdy klasyfikator stworzony za pomocą CNN, którego celem jest wykrywanie wzorców, przewiduje etykietę dla pojedynczej próbki bez uwzględnienia sąsiadujących próbek, CRF może uwzględniać kontekst \cite{crfcnn}. Aby to zrobić, prognoza jest modelowana jako model graficzny, który implementuje powiązania między przewidywaniami. W zależności od zastosowania wykorzystywane są różne modele grafów. Dla przykładu w przetwarzaniu języka naturalnego popularne są liniowe łańcuchy CRF, które implementują sekwencyjne zależności w prognozach. Podczas przetwarzania obrazu, wykres zazwyczaj łączy lokalizacje z pobliskimi i / lub podobnymi lokalizacjami, aby wymusić otrzymywanie podobnych prognoz.
	
Technika warunkowych pól losowych stanowi alternatywę dla technik uczenia głębokiego oraz daje możliwość hybrydowego łączenia tych dwóch podejść w celu uzyskania lepszych wyników segmentacji \cite{crfcnn}. Probabilistyczne modele graficzne są skuteczną metodą zwiększania dokładności zadań etykietowania na poziomie pikseli. Różne warianty warunkowych pól losowych odniosły powszechny sukces w tej dziedzinie i stały się jednym z najbardziej udanych modeli graficznych stosowanych w wizji komputerowej. Kluczową ideą prognozowania strukturalnego CRF dla klas semantycznych jest sformułowanie problemu przypisania etykiety jako probabilistycznego problemu wnioskowania, który obejmuje założenia, takie jak zgodność klas między podobnymi pikselami \cite{PyDense}. 
	

\section{Hybrydowe połączenie CRF i CNN}\label{crf_cnn_intro}	
Problemy niskiego poziomu widzenia komputerowego, takie jak semantyczna segmentacja obrazu lub ocena głębi, często wymagają przypisania etykiety każdemu pikselowi obrazu. Chociaż reprezentacja cech używana do klasyfikowania pojedynczych pikseli odgrywa ważną rolę w tym zadaniu, podobnie ważne jest, aby podczas przypisywania etykiet wziąć pod uwagę takie czynniki jak krawędzie obrazu, spójność wyglądu i spójność przestrzenna, aby uzyskać precyzyjne wyniki. Podejścia głębokiego uczenia takie jak głębokie sieci konwolucyjne odniosły ogromny sukces w wielu zadaniach widzenia komputerowego wysokiego poziomu, w zakresie rozpoznawania obrazu i wykrywania obiektów. Wciąż jednak pozostają mniej dokładne na poziomie pojedynczych pikseli. Ze względu na wykorzystywanie dużych pól receptywnych pojawiają się niedokładności związane z nieostrymi granicami będącymi wynikiem segmentacji. Należy również zwrócić uwagę na brak ograniczeń dotyczących płynności w CNN, które zachęcają do zgodności etykiet między podobnymi pikselami oraz spójności przestrzennej i wyglądu etykiety wyjściowej. Brak takich ograniczeń gładkości może skutkować słabym wytyczeniem granic obiektu i pojawianiem się małych obszarów niepożądanych w zadaniach segmentacji semantycznej.
	
Wnioskowanie CRF jest w stanie udoskonalić słabe i zgrubne przewidywania etykiet na poziomie piksela, aby uzyskać ostre granice i drobnoziarniste segmentacje. Dlatego też, CRF mogą być używane do przezwyciężania niedogodności w wykorzystywaniu CNN do zadań etykietowania na poziomie pikseli \cite{crfcnn}. W wyniku takich rozważań stworzony został kompletny system, który w pierwszej fazie przewiduje etykiety na poziomie piksela nie uwzględniając struktury, a następnie wykonuje oparte na CRF prognozowanie strukturalne wykorzystując do tego probablistyczne modelowanie graficzne. Po wstępnym wytrenowaniu modelu CNN, przychodzi czas na zastosowanie modułu CRF, który wykorzystując wynik przekazany z modelu CNN oraz obraz wejściowy przeprowadzi ponowną predykcję. Finalny proces przetwarzania obrazu zaprezentowany został na rysunku \ref{fig:crf_cnn}.

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm]{img/crf_cnn2}
	\caption[Schemat przedstawiający syntezę CNN z CRF]{Przetwarzanie obrazu za pomocą modelu CNN połączonego szeregowo z modułem CRF.}
	\label{fig:crf_cnn}
\end{figure}


\section{Metryki skuteczności}\label{metryki}
Systemy segmentacji semantycznej posiadają wiele możliwości na przedstawienie uzyskanej skuteczności stworzonych rozwiązań. Klasyczny współczynnik dokładności przypisania pikseli nie jest wystar-czający, ponieważ często otrzymane wyniki mogą dawać błędne wrażenie. Z tego powodu do opracowania wyników wykorzystane zostały również dwie dodatkowe metryki czyli IoU oraz F1 score.

\subsection{Dokładność przypisania pikseli}
Dokładność przypisania pikseli jest najprostszą koncepcyjnie metryką, wyrażaną w procentach. Wynik tej metryki informuje nas o liczbie pikseli, które zostały poprawnie przypisane do swojej klasy. Wynik wyznaczany tą metodą opisany jest wzorem:

\begin{equation}
	\label{eq:pixel_acc}
	acc = \frac{TP + TN}{TP + TN + FP + FN}
\end{equation}

gdzie $TP$ to liczba poprawnie zidentyfikowanych pikseli do sprawdzanej etykiety, natomiast $TN$ jest liczbą pikseli właściwie oznaczonych, które nie należą do sprawdzanej klasy. $FP$ i $FN$ odpowiadają za liczbę błędnie przypisanych pikseli do kontrolowanej klasy oraz pozostałych klas. Zgodnie z intuicją metryka ta może wydawać się skuteczna, jednak w praktyce otrzymywane wyniki potrafią bardzo często dawać błędne wrażenie na temat efektywności badanego model sieci. W przypadku, gdy powierzchnia klas semantycznych nie jest zrównoważona, czyli na obrazie dominuje jedna lub kilka klas, a inne zajmują tylko małą cześć obszaru, wynik opisanej metody może wskazywać na bardzo wysoką dokładność pomimo znikomej skuteczności sieci. Nierównowaga klas jest częstym problemem spotykanym w wielu zestawach danych ze świata rzeczywistego i pojawia się ona także w naszym przypadku projektowym. Dlatego w celu dokładniejszego pomiaru efektywności opracowanego systemu segmentacji zastosowane zostały dodatkowo dwie alternatywne metryki, które wykazują się większą skutecznością w ocenie rezultatów dla opisanego problemu.

\subsection{Indeks Jaccarda}\label{chap:iou} %tutaj
Metryka IoU (ang. \textit{Intersection-Over-Union}), znana również jako indeks Jaccarda, jest jedną z najczęściej wykorzystywanych metryk w algorytmach segmentacji semantycznej. Pomiar efektywności segmentacji przeprowadzony metodą IoU określa w jakim stopniu granica przewidziana podczas przewidywania pokrywa się z rzeczywistą granicą obiektu. W tym celu wyznaczana jest cześć wspólna wyniku działania systemu oraz faktycznego obszaru należącego do danej klasy, a następnie otrzymany rezultat dzielony jest przez sumę tych powierzchni na obu obrazach. Wizualizacja opisanych obliczeń zaprezentowana została na rysunku \ref{fig:iou}. Wynik dokładności mieści się w granicach od 0 do 1, gdzie 0 oznacza całkowity brak części wspólnej, natomiast 1 wskazuje na idealne dopasowanie segmentów.

\begin{figure}[h]
    \centering
	\includegraphics[width=0.45\textwidth]{img/IoU_schema}
	\caption[Wizualizacja IoU.]{Wizualizacja IoU.}
	\label{fig:iou}
\end{figure}

\subsection{F1 Score}\label{chap:f1score}
F1 score znany również pod nazwą współczynnika Dice jest sposobem określania skuteczności przewidywania, który jest dodatnio skorelowany z metryką IoU. Oznacza to, że wynik działania jednej metryki będzie znajdował swoje odzwierciedlenie w wyniku drugiej metryki. W rezultacie, jeśli przeprowadzone będzie badanie związane z wyborem najlepszego modelu, obie metryki wskażą tego samego zwycięzcę. Aby wyznaczyć współczynnik F1 score należy podwojoną część wspólną wyniku przewidywania oraz obrazu \textit{ground truth} podzielić przez sumę wszystkich pikseli na obu obrazach. Opisana zależność może zostać przedstawiona przy pomocy wzoru:

\begin{equation}
	\label{eq:fq_score}
	acc = \frac{2*TP}{2 * TP + FP + FN}
\end{equation}

\section{Podsumowanie}
W tym rozdziale przedstawione zostały podstawowe informacje związane z obserwacją Ziemi oraz technikami wykorzystywanymi do przetwarzania obrazu. W początkowych sekcjach można się dowiedzieć, że obecnie najczęściej stosowanymi czujnikami do satelitarnej obserwacji Ziemi są czujniki elektrooptyczne oraz radarowe z syntetyczną aparaturą. Natomiast czujniki filmowe wykorzystywane były głównie w przeszłości, a obecnie mogą służyć do uzupełniania danych pochodzących z innych źródeł. 	
	
W dalszej części pojawiają się informacje na temat uczenia maszynowego oraz wykorzystywanych w tej technologi technik. Istnieje wiele sposobów uczenia sieci neuronowych, jednak uwagę należy zwrócić na te które obecnie dominują w dziedzinie segmentacji obrazu i wykrywaniu na nim określonych obiektów. Analizując te zagadnienia można dowiedzieć się, że posiadając wiedzę na temat oczekiwanych rezultatów, skutecznym rozwiązaniem będzie wykorzystanie nadzorowanych technik uczenia się. Warto także skorzystać z transferu wiedzy poprzez użycie wytrenowanego już wstępnie modelu sieci neuronowej, a następnie zaadoptowanie go do realizacji specjalistycznego zadania. Temat sieci neuronowych zakończony zostaje deskrypcją splotowych sieci neuronowych, które obecnie dominują w dziedzinie AI (ang. \textit{Artificial Intelligence}) związaną z segmentacją obrazu. Kolejnym omawianym w tym rozdziale sposobem przetwarzania informacji graficznych jest technika warunkowych pól losowych CRF, która podczas przeprowadzania przewidywania pozwala na uwzględnianie zgodności etykiet pomiędzy podobnymi pikselami. Zaprezentowane zostały również techniki pozwalające na pomiar skuteczności dla problemów semantycznej segmentacji. W wyniku przeprowadzenia analizy ich działania, wywnioskowano że klasyczny współczynnik dokładności przypisania pikseli może nie być wystarczający i w celu rzetelnego ocenienia skuteczność opracowanego systemu warto skorzystać z metryki IoU lub F1 score.
	