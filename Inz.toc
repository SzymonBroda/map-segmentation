\babel@toc {polish}{}
\contentsline {chapter}{Spis tre\IeC {\'s}ci}{4}{chapter*.1}
\contentsline {chapter}{\numberline {1}WST\IeC {\k E}P I CEL PRACY}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Cel Pracy}{5}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Uk\IeC {\l }ad Pracy}{5}{subsection.1.1.1}
\contentsline {chapter}{\numberline {2}Wprowadzenie}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Obserwacja Ziemi}{6}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Rodzaje wykorzystywanych czujnik\IeC {\'o}w}{6}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Orbity}{7}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Uczenie maszynowe}{8}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Uczenie nadzorowane}{8}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Uczenie bez nadzoru}{8}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Uczenie ze wzmocnieniem}{8}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Sieci neuronowe}{9}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Historia sieci neuronowych}{9}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Budowa neuronu}{10}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Wielowarstwowe sieci neuronowe}{11}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Uczenie transferowe}{12}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Uczenie wielozadaniowe}{12}{subsection.2.4.1}
\contentsline {section}{\numberline {2.5}Uczenie g\IeC {\l }\IeC {\k e}bokie}{13}{section.2.5}
\contentsline {section}{\numberline {2.6}Splotowe sieci neuronowe}{13}{section.2.6}
\contentsline {section}{\numberline {2.7}Warunkowe pola losowe}{14}{section.2.7}
\contentsline {section}{\numberline {2.8}Hybrydowe po\IeC {\l }\IeC {\k a}czenie CRF-CNN}{14}{section.2.8}
\contentsline {chapter}{\numberline {3}Przegl\IeC {\k a}d podobnych rozwi\IeC {\k a}za\IeC {\'n}}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Systemy klasyfikowania obraz\IeC {\'o}w}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}AlexNet}{15}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}VGG-16}{15}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}GoogLeNet}{15}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}ResNet}{15}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Por\IeC {\'o}wnanie modeli sieci}{16}{subsection.3.1.5}
\contentsline {section}{\numberline {3.2}Systemy segmentacji semantycznej}{17}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Fully Convolutional Network}{17}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}U-Net}{18}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}DeepLab}{19}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Systemy segmentacji obraz\IeC {\'o}w lotniczych i satelitarnych}{19}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Wykrywanie drogi przy u\IeC {\.z}yciu FCN}{19}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Segmentacja obrazu wysokiej rozdzielczo\IeC {\'s}ci FCN}{20}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Segmentacja obrazu wysokiej rozdzielczo\IeC {\'s}ci DeepLab}{21}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Segmentacja budynk\IeC {\'o}w}{21}{subsection.3.3.4}
\contentsline {chapter}{\numberline {4}Implementacja}{22}{chapter.4}
\contentsline {section}{\numberline {4.1}Narz\IeC {\k e}dzia}{22}{section.4.1}
\contentsline {section}{\numberline {4.2}Przygotowanie danych}{23}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Dataset INRIA}{23}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Przygotowanie zbioru treningowego}{23}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Detekcja budynk\IeC {\'o}w}{24}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Model sieci}{24}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Trening sieci}{25}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Przetwarzanie ko\IeC {\'n}cowe}{25}{section.4.4}
\contentsline {section}{\numberline {4.5}Dynamiczne przetwarzanie mapy}{26}{section.4.5}
\contentsline {chapter}{Spis rysunk\IeC {\'o}w}{28}{chapter*.22}
\contentsline {chapter}{Spis tabel}{29}{chapter*.23}
